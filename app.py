from RandomDealData import RandomDealData
from flask import Flask, Response
from flask_cors import CORS

#code review approved 

app = Flask(__name__)
CORS(app)

@app.route("/")
def stream():
    def event_stream():
        while True:
            random_deal_data = RandomDealData()
            instrument_list = random_deal_data.createInstrumentList()
            yield "data: " + random_deal_data.createRandomData(instrument_list) + "\n\n"

    return Response(event_stream(), mimetype="text/event-stream")

if __name__ == "__main__":
    app.run("0.0.0.0", port=8080, debug=True)
    # app.run("0.0.0.0", port=3005, debug=True)
    # app.run("localhost", port=3005, debug=True)
