import unittest
import random
from Instrument import *

#code reviewed

class Testing(unittest.TestCase):

    def test_init(self):

        myObj = Instrument("name", 0.01, 0.02, 0.03)
         
        assert myObj.name == "name"
        assert myObj._Instrument__price == 0.01
        assert myObj._Instrument__startingPrice == 0.01
        assert myObj._Instrument__drift == 0.02
        assert myObj._Instrument__variance == 0.03

    def test_calculateNextPrice(self):

        d = {'B': 1.01, 'S': 0.99}

        for c in d.keys():
            myObj = Instrument("name", random.random(), 0.00, 0.00)
            price = myObj.calculateNextPrice(c) / d[c]
            assert price == myObj._Instrument__startingPrice     

if __name__ == '__main__':
    unittest.main()
