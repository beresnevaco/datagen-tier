import unittest
import random
from Instrument import *
from RandomDealData import *
import tracemalloc

#code reviewed

class Testing(unittest.TestCase):

    tracemalloc.start()

    def test_createInstrumentList(self):
 
        myObj = RandomDealData().createInstrumentList()
        assert type(myObj) == list

        for obj in myObj:
            assert type(obj) == Instrument

    def test_createRandomData(self):

        instrumentList = [Instrument("name", 0.01, 0.02, 0.03)]
        myObj = RandomDealData().createRandomData(instrumentList)

        assert type(myObj) == str
        
        l = ['instrumentName', 'name', 'cpty', 'price', 'type','quantity', 'time']

        for string in l:
            assert string in myObj

if __name__ == '__main__':
    unittest.main()
